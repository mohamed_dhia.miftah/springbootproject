package tn.enis.SpringProject.service;

import java.util.List;

import tn.enis.SpringProject.entities.Patient;

public interface IPatientService {

	public Patient getOnePatient(Long id);

	public List<Patient> getAllPatient();

	public void addPatient(Patient patient);

	public void updatePatient(Long id, Patient patient);

	public String deletePatient(Long id);

	public List<Patient> findPatientByLastName(String nom);

	public List<Patient> findPatientByName(String prenom);
	
	public List<Patient> sortByLastName();

	public List<Patient> sortByName();

}
