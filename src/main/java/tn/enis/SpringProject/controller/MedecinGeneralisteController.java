package tn.enis.SpringProject.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tn.enis.SpringProject.entities.MedecinGeneraliste;
import tn.enis.SpringProject.service.MedecinGeneralisteService;

@RestController
@RequestMapping("/cabinet")
@CrossOrigin("*")
public class MedecinGeneralisteController {

	@Autowired
	MedecinGeneralisteService medecinGeneralisteService;

	@GetMapping(value = "/getOneMedecinGeneraliste/{id}")
	public MedecinGeneraliste getOneMedecinGeneraliste(@PathVariable Long id) {
		return medecinGeneralisteService.getOneMedecinGeneraliste(id);
	}

	@GetMapping("/getAllMedecinGeneraliste")
	public List<MedecinGeneraliste> getAllMedecinGeneraliste() {
		return medecinGeneralisteService.getAllMedecinGeneraliste();
	}

	@PostMapping("/addMedecinGeneraliste")
	public void addMedecinGeneraliste(@RequestBody MedecinGeneraliste medecinGeneraliste) {
		medecinGeneralisteService.addMedecinGeneraliste(medecinGeneraliste);
	}

	@PutMapping(value = "/updateMedecinGeneraliste/{id}")
	public void UpdateMedecinGeneraliste(@PathVariable Long id, @RequestBody MedecinGeneraliste medecinGeneraliste) {
		medecinGeneralisteService.updateMedecinGeneraliste(id, medecinGeneraliste);
	}

	@DeleteMapping("/deleteMedecinGeneraliste/{id}")
	public void deleteMedecinGeneraliste(@PathVariable Long id) {
		medecinGeneralisteService.deleteMedecinGeneraliste(id);
	}
}
