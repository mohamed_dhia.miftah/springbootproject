package tn.enis.SpringProject.service;

import java.util.List;

import tn.enis.SpringProject.entities.MedecinGeneraliste;

public interface IMedecinGeneralisteService {

	public MedecinGeneraliste getOneMedecinGeneraliste(Long id);

	public List<MedecinGeneraliste> getAllMedecinGeneraliste();

	public void addMedecinGeneraliste(MedecinGeneraliste medecinGeneraliste);

	public void updateMedecinGeneraliste(Long id, MedecinGeneraliste medecinGeneraliste);

	public String deleteMedecinGeneraliste(Long id);

}
