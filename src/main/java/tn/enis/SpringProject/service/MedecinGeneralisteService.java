package tn.enis.SpringProject.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.enis.SpringProject.dao.MedecinGeneralisteRepository;
import tn.enis.SpringProject.entities.MedecinGeneraliste;

@Service
public class MedecinGeneralisteService implements IMedecinGeneralisteService {

	@Autowired
	MedecinGeneralisteRepository medecinGeneralisteRepository;

	@Override
	public MedecinGeneraliste getOneMedecinGeneraliste(Long id) {
		return medecinGeneralisteRepository.findById(id).get();
	}

	@Override
	public List<MedecinGeneraliste> getAllMedecinGeneraliste() {
		return medecinGeneralisteRepository.findAll();
	}

	@Override
	public void addMedecinGeneraliste(MedecinGeneraliste medecinGeneraliste) {
		medecinGeneralisteRepository.saveAndFlush(medecinGeneraliste);
	}

	@Override
	public void updateMedecinGeneraliste(Long id, MedecinGeneraliste medecinGeneraliste) {
		medecinGeneraliste.setId(id);
		medecinGeneralisteRepository.saveAndFlush(medecinGeneraliste);
	}

	@Override
	public String deleteMedecinGeneraliste(Long id) {
		medecinGeneralisteRepository.deleteById(id);
		medecinGeneralisteRepository.flush();
		return "medecin generaliste deleted";
	}
}
