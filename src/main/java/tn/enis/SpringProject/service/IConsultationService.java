package tn.enis.SpringProject.service;

import java.util.List;

import tn.enis.SpringProject.entities.Consultation;

public interface IConsultationService {

	public Consultation getOneConsultation(Long id);

	public List<Consultation> getAllConsultation();

	public void addConsultation(Consultation consultation);

	public void updateConsultation(Long id, Consultation consultation);

	public String deleteConsultation(Long id);
	
	public List<Consultation> sortByDate();


}
